package com.myspring.bookstory;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.myspring.bookstory.entity.GoodsVO;
import com.myspring.bookstory.service.GoodsService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private GoodsService goodsService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model, HttpSession session) {
		logger.info("Welcome home! The client locale is {}.", locale);
		Map<String, List<GoodsVO>> goodsMap = goodsService.listGoods();
//		List<GoodsVO> list = Arrays.asList(null, null, null, null, null);
		
		session.removeAttribute("side_menu");
		
		
		model.addAttribute("goodsMap", goodsMap);
		return "home";
	}
	
}
