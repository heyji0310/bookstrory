package com.myspring.bookstory.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.myspring.bookstory.entity.MemberVO;
import com.myspring.bookstory.entity.SearchVO;
import com.myspring.bookstory.service.AdminMemberService;

@Controller("adminMemberController")
@RequestMapping(value = "/admin/member")
public class AdminMemberController {

	@Autowired
	private AdminMemberService adminMemberService;
	
	@RequestMapping(value = "/adminMemberMain.do", method = RequestMethod.GET)
	public ModelAndView adminMemberMain(@RequestParam Map<String, Object> adminMemberMap, HttpServletRequest request ) {
		ModelAndView mav = new ModelAndView("/admin/member/adminMemberMain");
		
		HttpSession session=request.getSession();
		session=request.getSession();
		session.setAttribute("side_menu", "admin_mode");
		
		return mav;
	}

	@RequestMapping(value="/adminMemberList.do", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
	public Map<String, Object> listJSON (@ModelAttribute("searchVO") SearchVO searchVO, HttpServletRequest request) throws Exception {
		
    	//DataTable 검색인자 설정
		searchVO.setOrderColumn(request.getParameter("order[0][column]"));
		searchVO.setDir(request.getParameter("order[0][dir]"));
    	
		//상위 코드 목록 얻기 
		return adminMemberService.searchMemberList(searchVO);

	}
	
	@RequestMapping(value = "/adminMemberDetail.do", method = RequestMethod.GET)
	public ModelAndView adminMemberDetail(@RequestParam Map<String, Object> adminMemberMap, HttpServletRequest request ) {
		ModelAndView mav = new ModelAndView("/admin/member/adminMemberDetail");
		
		return mav;
	}
	
	@RequestMapping(value = "/changeAccountStatus.do", method = RequestMethod.POST)
	public MemberVO changeAccountStatus(@RequestBody MemberVO adminMemberMap, HttpServletRequest request ) {
		MemberVO ret = new MemberVO();
		System.out.println("들어오나 컨트롤러");
		try {
			ret = adminMemberService.changeAccountStatus(adminMemberMap);
			ret.setResultCd("SUCC");
		} catch (Exception e) {
			ret.setResultCd("FAIL");
			e.printStackTrace();
		}
		
		return ret;
	}
}
