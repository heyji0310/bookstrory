package com.myspring.bookstory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.myspring.bookstory.entity.GoodsVO;
import com.myspring.bookstory.service.GoodsService;

@Controller
@RequestMapping("/goods")
public class GoodsController {
	
	@Autowired
	private GoodsService goodsService;
	
	@RequestMapping("/goodsDetail.do/{goods_id}")
	public ModelAndView view(@PathVariable int goods_id) {
		ModelAndView mav = new ModelAndView("/view/goods");
		GoodsVO goodsVO = goodsService.findOneById(goods_id);
		if (goodsVO == null) {
			// OR Optional<GoodsVO>
			mav.setViewName("/error/404");
		}
		mav.addObject("goodsVO", goodsVO);
		return mav;
	}
}
