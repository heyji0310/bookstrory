package com.myspring.bookstory.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.myspring.bookstory.entity.MemberVO;
import com.myspring.bookstory.service.MemberService;

@Controller("memberController")
@RequestMapping(value = "/member")
public class MemberController {

	@Autowired
	private MemberService memberService;
	
	@RequestMapping(value = "/loginForm.do", method = RequestMethod.GET)
	public ModelAndView loginForm(@RequestParam Map<String, String> loginMap) throws Exception {
		ModelAndView mav = new ModelAndView("/member/loginForm");
		return mav;
	}

	@RequestMapping(value = "/findIdForm.do", method = RequestMethod.GET)
	public ModelAndView findIdForm() throws Exception {
		ModelAndView mav = new ModelAndView("/member/findIdForm");
		return mav;
	}
	
	@RequestMapping(value = "/findId.do", method = RequestMethod.POST)
	public ModelAndView findId(@RequestParam Map<String, String> findIdMap) throws Exception {
		System.out.println("findIdMap" + findIdMap);
		ModelAndView mav = new ModelAndView("/member/findIdResult");
		MemberVO member = memberService.findId(findIdMap);
		if (member != null) {
			mav.addObject("status", true);
			mav.addObject("message", "아이디는 " + member.getMember_id() + " 입니다");
		} else {
			mav.addObject("status", false);
			mav.addObject("message", "찾으시는 정보가 올바르지 않습니다.");
		}
		return mav;
	}
	
	@RequestMapping(value = "/findPwdForm.do", method = RequestMethod.GET)
	public ModelAndView findPwdForm() throws Exception {
		ModelAndView mav = new ModelAndView("/member/findPwdForm");
		return mav;
	}
	
	@RequestMapping(value = "/findPwd.do", method = RequestMethod.POST)
	public ModelAndView findPwd(@RequestParam Map<String, String> findPwdMap) throws Exception {
		System.out.println("findPwdMap" + findPwdMap);
		ModelAndView mav = new ModelAndView("/member/findPwdResult");
		MemberVO member = memberService.findPwd(findPwdMap);
		if (member != null) {
			mav.addObject("status", true);
			mav.addObject("message", "비밀번호는 " + member.getMember_pw() + " 입니다");
		} else {
			mav.addObject("status", false);
			mav.addObject("message", "찾으시는 정보가 올바르지 않습니다.");
		}
		return mav;
	}
	
	@RequestMapping(value = "/registerForm.do", method = RequestMethod.GET)
	public ModelAndView registerForm() throws Exception {
		ModelAndView mav = new ModelAndView("/member/registerForm");
		return mav;
	}
	
	@RequestMapping(value = "/addMember.do", method = RequestMethod.POST)
	public ResponseEntity<String> addMember(@ModelAttribute("memberVO") MemberVO _memberVO, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("utf-8");
		String message = null;
		ResponseEntity<String> resEntity = null;
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");
		try {
			memberService.addMember(_memberVO);
			message = "<script>";
			message += " alert('회원가입을 환영합니다.');";
			message += " location.href='" + request.getContextPath() + "/member/loginForm.do';";
			message += " </script>";

		} catch (Exception e) {
			message = "<script>";
			message += " alert('회원 정보를 다시 확인해 주세요');";
			message += " location.href='" + request.getContextPath() + "/member/registerForm.do';";
			message += " </script>";
			e.printStackTrace();
		}
		resEntity = new ResponseEntity<>(message, responseHeaders, HttpStatus.OK);
		return resEntity;
	}
	
	@RequestMapping(value="/overlapped.do" ,method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> overlapped(@RequestBody  MemberVO _memberVO, HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> result = new HashMap<>();
		int count = memberService.overlapped(_memberVO);
		result.put("status", 0 == count);
		result.put("message", 0 == count ? "사용가능한 아이디 입니다" : "사용 불가능한 아이디 입니다");
		
		return result;
	}
	
	@RequestMapping(value = "/withdrawalForm.do", method = RequestMethod.GET)
	public ModelAndView withdrawalForm() throws Exception {
		ModelAndView mav = new ModelAndView("/member/withdrawalForm");
		return mav;
	}
		
	@RequestMapping(value="/withdrawal.do" ,method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> withdrawal(@RequestBody MemberVO _memberVO, HttpServletRequest request) {
		Map<String, Object> result = new HashMap<>();
		System.out.println("회원탈퇴~");
		int count = memberService.withdrawal(_memberVO);
		result.put("status", 1 == count);
		result.put("message", 1 == count ? "탈퇴 성공했습니다" : "탈퇴 실패했습니다");

		HttpSession session = request.getSession();
		session.setAttribute("isLogOn", false);
		session.removeAttribute("member");
		
		result.put("url", "");
		return result;
	}
	
}
