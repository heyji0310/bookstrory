package com.myspring.bookstory.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.myspring.bookstory.entity.MemberVO;
import com.myspring.bookstory.service.MemberService;

@RestController
@RequestMapping("/member")
public class MemberRestController {

	@Autowired
	private MemberService memberService;

//	@RequestMapping("/login.do")
//	public Map<String, Object> login(@RequestBody HashMap<String, String> loginInfo, HttpServletRequest request) {
//		MemberVO member = memberService.login(loginInfo);
//		HttpSession session = request.getSession();
//		Map<String, Object> ret = new HashMap<>();
//
//		if (member != null) { // login 성공
//			session.setAttribute("member", member);
//			ret.put("message", "환영합니다!");
//			ret.put("url", "/bookstory");
//		} else {
//			ret.put("message", "다시 시도해 주세요!");
//		}
//		ret.put("result", member != null);
//		return ret;
//	}

	@RequestMapping(value = "/login.do", method = RequestMethod.POST)
	public Map<String, Object> login(@RequestBody HashMap<String, String> loginInfo, HttpServletRequest request)
			throws Exception {
		MemberVO member = memberService.login(loginInfo);
		
		HttpSession session = request.getSession();
		Map<String, Object> ret = new HashMap<>();

		if (member != null && member.getMember_id() != null) { // login 성공
			session = request.getSession();
			session.setAttribute("isLogOn", true);
			session.setAttribute("member", member);
			ret.put("message", "환영합니다!");
			ret.put("url", "/bookstory");
		} else {
			ret.put("message", "계정 정보가 없습니다.");
		}
		ret.put("result", member != null);
		return ret;
	}

	@RequestMapping(value = "/logout.do", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		HttpSession session = request.getSession();
		session.setAttribute("isLogOn", false);
		session.removeAttribute("member");
		mav.setViewName("redirect:/");
		return mav;
	}

	
}
