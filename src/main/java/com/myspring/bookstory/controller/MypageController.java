package com.myspring.bookstory.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.myspring.bookstory.entity.MemberVO;
import com.myspring.bookstory.service.MypageService;

@Controller("mypageController")
@RequestMapping(value = "/mypage")
public class MypageController {

	@Autowired
	private MypageService mypageService;
	
	@RequestMapping(value = "/myPageMain.do", method = RequestMethod.GET)
	public ModelAndView myPageMain(@RequestParam(required = false, value = "message") String message,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		session = request.getSession();
		session.setAttribute("side_menu", "my_page"); // 마이페이지 사이드 메뉴로 설정한다.

		ModelAndView mav = new ModelAndView("/mypage/myPageMain");

		MemberVO memberVO = (MemberVO) session.getAttribute("member");
		mav.addObject("member", memberVO);
		mav.addObject("message", message);

		return mav;
	}

	@RequestMapping(value = "/myDetailInfo.do", method = RequestMethod.GET)
	public ModelAndView myDetailInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("/mypage/myDetailInfo");
		return mav;
	}

	@RequestMapping(value = "/modifyMyInfo.do", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> modifyMyInfo(@RequestParam("attribute") String attribute, @RequestParam("value") String value, HttpServletRequest request) {

		Map<String, Object> memberMap = new HashMap<String, Object>();
		String val[] = null;
		HttpSession session = request.getSession();
		MemberVO memberVO = (MemberVO) session.getAttribute("member");
		String member_id = memberVO.getMember_id();
		if (attribute.equals("member_birth")) {
			val = value.split(",");
			memberMap.put("member_birth", val[0]);
			memberMap.put("member_birth_gn", val[1]);
		} else if (attribute.equals("phone")) {
			val = value.split(",");
			memberMap.put("phone", val[0]);
			memberMap.put("smssts_yn", val[1]);
		} else if (attribute.equals("email")) {
			val = value.split(",");
			memberMap.put("email", val[0]);
			memberMap.put("emailsts_yn", val[1]);
		} else if (attribute.equals("address")) {
			val = value.split(",");
			memberMap.put("zipcode", val[0]);
			memberMap.put("roadAddress", val[1]);
			memberMap.put("jibunAddress", val[2]);
			memberMap.put("namujiAddress", val[3]);
		} else {
			memberMap.put(attribute, value);
		}

		memberMap.put("member_id", member_id);

		// 수정된 회원 정보를 다시 세션에 저장한다.
		int count = mypageService.modifyMyInfo(memberMap);
		session.removeAttribute("memberInfo");
		//session.setAttribute("memberInfo", memberVO);

		Map<String, Object> result = new HashMap<>();
		result.put("status", 0 != count);
		result.put("message", 0 != count ? "수정 성공했습니다" : "수정 실패했습니다");
		result.put("url", "");
		return result;
		
//		String message = null;
//		ResponseEntity<String> resEntity = null;
//		HttpHeaders responseHeaders = new HttpHeaders();
//		message = "mod_success";
//		resEntity = new ResponseEntity(message, responseHeaders, HttpStatus.OK);
//		return resEntity;
	}

}
