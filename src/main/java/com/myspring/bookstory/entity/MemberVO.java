package com.myspring.bookstory.entity;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MemberVO {
	private String member_id;
	private String member_pw;
	private String member_name;
	private String member_gender;
	private String member_birth;
	private String member_birth_gn;
	private String tel;
	private String phone;
	private String smssts_yn;
	private String email;
	private String emailsts_yn;
	private String zipcode;
	private String roadAddress;
	private String jibunAddress;
	private String namujiAddress;
	private String joinDate;
	private String del_yn;
	private String loginId;
	private String resultCd;
	
	public List<String> getStringList(int numId) {
		List<String> stringList = new ArrayList<String>();
		
		stringList.add(String.valueOf(numId));
		stringList.add(member_id);
		stringList.add(member_name);
		stringList.add(phone);
		stringList.add(jibunAddress);
		stringList.add(joinDate);
		stringList.add(del_yn);
		stringList.add(member_id);
		
		return stringList;
	}
	
	
}
