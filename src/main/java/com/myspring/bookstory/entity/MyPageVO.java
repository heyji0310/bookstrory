package com.myspring.bookstory.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MyPageVO {
	private String member_id;
	private String beginDate;
	private String endDate;
}
