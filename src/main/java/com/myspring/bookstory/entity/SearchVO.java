package com.myspring.bookstory.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchVO {
	private String draw;
	private int start;
	private int length;
	private String orderColumn;
	private String dir;
	private String searchKeyword;
}
