package com.myspring.bookstory.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.myspring.bookstory.entity.MemberVO;
import com.myspring.bookstory.entity.SearchVO;

@Repository("adminMemberDAO")
public interface AdminMemberRepository {

	List<MemberVO> memberList();
	
	List<MemberVO> searchMemberList(SearchVO searchVO);

	int getSearchMemberTotalCount(SearchVO searchVO);
	
	MemberVO changeAccountStatus(MemberVO memberVO);
}
