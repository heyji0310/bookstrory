package com.myspring.bookstory.repository;

import java.util.List;
import java.util.Map;

import com.myspring.bookstory.entity.GoodsVO;
import com.myspring.bookstory.entity.ImageFileVO;

public interface GoodsRepository {
	List<GoodsVO> selectGoodsList(String goodsStatus);
	Map<String,List<GoodsVO>> listGoods();
	
	Map<String, Object> goodsDetail(String _goods_id);
	GoodsVO findOneById(int goods_id);
	List<ImageFileVO> selectGoodsDetailImage(String goods_id);
}
