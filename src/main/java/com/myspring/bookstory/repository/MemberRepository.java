package com.myspring.bookstory.repository;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.myspring.bookstory.entity.MemberVO;

@Repository("memberDAO")
public interface MemberRepository {

	MemberVO login(Map<String, String> loginMap);
	
	MemberVO selectOne(Map<String, String> loginMap);
	
	MemberVO findId(Map<String, String> findIdMap);
	
	MemberVO findPwd(Map<String, String> findPwdMap);
	
	MemberVO register(Map<String, String> loginMap);
	
	int selectOverlappedID(MemberVO memberVO);
	
	int addMember(MemberVO memberVO);
	
	int withdrawal(MemberVO _memberVO);
}
