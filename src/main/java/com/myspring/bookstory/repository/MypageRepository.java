package com.myspring.bookstory.repository;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.myspring.bookstory.entity.MemberVO;

@Repository("mypageDAO")
public interface MypageRepository {

	
	MemberVO selectMyDetailInfo(String member_id);
	
	MemberVO selectMyOrderGoodsList(String member_id);
	
	int updateMyInfo(Map<String, Object> _memberVO);
	
}
