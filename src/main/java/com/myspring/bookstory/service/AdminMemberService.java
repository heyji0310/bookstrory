package com.myspring.bookstory.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.myspring.bookstory.entity.MemberVO;
import com.myspring.bookstory.entity.SearchVO;
import com.myspring.bookstory.repository.AdminMemberRepository;

@Service("adminMemberService")
@Transactional(propagation=Propagation.REQUIRED)
public class AdminMemberService {

	@Autowired
	private AdminMemberRepository adminMemberRepository;

	public List<MemberVO> memberList() {
		return adminMemberRepository.memberList();
	}
	
	public Map<String, Object> searchMemberList(SearchVO searchVO) {

        List<List<String>> array = new ArrayList<List<String>>();
		int start = searchVO.getStart();
        for (MemberVO item : adminMemberRepository.searchMemberList(searchVO)) {
        	array.add(item.getStringList(++start));
		}
        
        int recordsTotal = adminMemberRepository.getSearchMemberTotalCount(searchVO);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("recordsTotal", recordsTotal);
		result.put("recordsFiltered", recordsTotal);
		result.put("data", array);
		
		return result;
	}
	
	public MemberVO changeAccountStatus(MemberVO memberVO) {
		System.out.println("들어오나 서비스");
		MemberVO retVO = new MemberVO();
		try {
			adminMemberRepository.changeAccountStatus(memberVO);
			retVO.setResultCd("SUCC");
		} catch (Exception e) {
			e.printStackTrace();
			retVO.setResultCd("FAIL");
		} 
		
		return retVO;
	}
	

}
