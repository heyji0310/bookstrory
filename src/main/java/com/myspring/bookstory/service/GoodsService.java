package com.myspring.bookstory.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myspring.bookstory.entity.GoodsVO;
import com.myspring.bookstory.repository.GoodsRepository;

@Service
public class GoodsService {
	@Autowired
	private GoodsRepository goodsRepository;
	@Autowired
	private SqlSession sqlSession;
	
	public Map<String, List<GoodsVO>> listGoods() {
		Map<String, List<GoodsVO>> goodsMap = new HashMap<String, List<GoodsVO>>();
		List<GoodsVO> goodsList = goodsRepository.selectGoodsList("1");
		goodsMap.put("bestseller", goodsList);
		goodsList = goodsRepository.selectGoodsList("2");
		goodsMap.put("newbook", goodsList);

		goodsList = goodsRepository.selectGoodsList("3");
		goodsMap.put("steadyseller", goodsList);
		return goodsMap;
	}
	
	public List<GoodsVO> selectGoodsList(String goodsStatus ) {
		List<GoodsVO> goodsList = (ArrayList) sqlSession.selectList("mapper.goods.selectGoodsList",goodsStatus);
	   return goodsList;	
     
	}
	
//	public Map goodsDetail(String _goods_id) {
//		Map goodsMap=new HashMap();
//		GoodsVO goodsVO = goodsRepository.selectGoodsDetail(_goods_id);
//		goodsMap.put("goodsVO", goodsVO);
//		List<ImageFileVO> imageList =goodsRepository.selectGoodsDetailImage(_goods_id);
//		goodsMap.put("imageList", imageList);
//		return goodsMap;
//	}
	
	public GoodsVO findOneById(int goods_id) {
		return goodsRepository.findOneById(goods_id);
	}
}
