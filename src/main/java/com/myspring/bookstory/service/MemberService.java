package com.myspring.bookstory.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.myspring.bookstory.entity.MemberVO;
import com.myspring.bookstory.repository.MemberRepository;

@Service("memberService")
@Transactional(propagation=Propagation.REQUIRED)
public class MemberService {

	@Autowired
	private MemberRepository memberRepository;

	public MemberVO login(Map<String, String> loginMap) {
		return memberRepository.login(loginMap);
	}
	
	public int overlapped(MemberVO memberVO) {
		return memberRepository.selectOverlappedID(memberVO);
	}
	
	public MemberVO findId(Map<String, String> findIdMap) {
		return memberRepository.findId(findIdMap);
	}
	
	public MemberVO findPwd(Map<String, String> findPwdMap) {
		return memberRepository.findPwd(findPwdMap);
	}

	public MemberVO register(Map<String, String> loginMap) {
		return memberRepository.register(loginMap);
	}
	
	public int addMember(MemberVO memberVO) {
		return memberRepository.addMember(memberVO);
	}
	
	public int withdrawal(MemberVO _memberVO) {
		return memberRepository.withdrawal(_memberVO);
	}

}
