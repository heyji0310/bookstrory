package com.myspring.bookstory.service;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.myspring.bookstory.entity.MemberVO;
import com.myspring.bookstory.repository.MypageRepository;

@Service("mypageService")
@Transactional(propagation = Propagation.REQUIRED)
public class MypageService {

	@Autowired
	private MypageRepository mypageRepository;

	@Autowired
	private SqlSession sqlSession;

	public MemberVO myDetailInfo(String member_id) {
		return mypageRepository.selectMyDetailInfo(member_id);
	}

	public MemberVO selectMyDetailInfo(String member_id) {
		MemberVO memberVO = (MemberVO) sqlSession.selectOne("mapper.mypage.selectMyDetailInfo", member_id);
		return memberVO;
	}

	public int modifyMyInfo(Map<String, Object> _memberVO) {
		return mypageRepository.updateMyInfo(_memberVO);
	}


}
