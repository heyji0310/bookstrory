<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<%
request.setCharacterEncoding("UTF-8");
%>


<h3>회원관리</h3>
<br/>

<form name="searchForm" id="searchForm" method="post"
	onsubmit="$memberTable.draw(); return false;">
	<div class="panel search-panel">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-11">
					<input class="form-control" placeholder="이름을 입력해주세요" type="text"
						id="searchKeyword" name="searchKeyword"
						value="${searchVO.searchKeyword}">
					<input type="submit" class="btn btn-block btn-primary btnSearch"
						value="조회" />
				</div>
			</div>
		</div>
	</div>
</form>
<!-- END Search Form -->
<br/>
<table id="memberTable" class="display table-bordered memberTable" style="width:100%;">
	<thead>
		<tr>
			<th>번호</th>
			<th>회원아이디</th>
			<th>회원이름</th>
			<th>휴대폰번호</th>
			<th>주소</th>
			<th>가입일</th>
			<th>계정상태</th>
			<th>계정제한</th>
		</tr>
	</thead>
</table>

<br/><br/><br/>

<%-- 
<H3>회원 조회</H3>
<form name="frm_delivery_list">
	<table cellpadding="10" cellspacing="10">
		<tbody>

			<tr>
				<td><select name="s_search_type">
						<option value="all" checked>전체</option>
						<option value="member_name">회원이름</option>
						<option value="member_id">회원아이디</option>
						<option value="phone">휴대폰번호</option>
						<option value="jibunAddress">회원주소</option>
				</select> <input type="text" size="30" name="t_search_word" /> <input
					type="button" value="조회" name="btn_search"
					onClick="fn_detail_search()" /></td>
			</tr>
		</tbody>
	</table>
	<div class="clear"></div>

	<div class="clear"></div>
	<table class="list_view">
		<tbody align=center>
			<tr align=center bgcolor="#ffcc00">
				<td class="fixed">회원아이디</td>
				<td class="fixed">회원이름</td>
				<td>휴대폰번호</td>
				<td>주소</td>
				<td>가입일</td>
				<td>탈퇴여부</td>
			</tr>
			<c:choose>
				<c:when test="${empty member_list}">
					<tr>
						<td colspan=5 class="fixed"><strong>조회된 회원이 없습니다.</strong></td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="item" items="${member_list}" varStatus="item_num">
						<tr>
							<td width=10%><a
								href="${pageContext.request.contextPath}/admin/member/memberDetail.do?member_id=${item.member_id}">
									<strong>${item.member_id}</strong>
							</a></td>
							<td width=10%><strong>${item.member_name}</strong><br>
							</td>
							<td width=10%><strong>${item.phone}</strong><br></td>
							<td width=50%><strong>${item.roadAddress}</strong><br>
								<strong>${item.jibunAddress}</strong><br> <strong>${item.namujiAddress}</strong><br>
							</td>
							<td width=10%><strong>${item.joinDate}</strong><br></td>
							<td width=10%><c:choose>
									<c:when test="${item.del_yn=='N' }">
										<strong>활동중</strong>
									</c:when>
									<c:otherwise>
										<strong>탈퇴</strong>
									</c:otherwise>
								</c:choose></td>
						</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
			<tr>
				<td colspan=8 class="fixed"><c:forEach var="page" begin="1"
						end="10" step="1">
						<c:if test="${chapter >1 && page==1 }">
							<a
								href="${pageContext.request.contextPath}/admin/member/adminMemberMain.do?chapter=${chapter-1}&pageNum=${(chapter-1)*10 +1 }">&nbsp;pre
								&nbsp;</a>
						</c:if>
						<a
							href="${pageContext.request.contextPath}/admin/member/adminMemberMain.do?chapter=${chapter}&pageNum=${page}">${(chapter-1)*10 +page }
						</a>
						<c:if test="${page ==10 }">
							<a
								href="${pageContext.request.contextPath}/admin/member/adminMemberMain.do?chapter=${chapter+1}&pageNum=${chapter*10+1}">&nbsp;
								next</a>
						</c:if>
					</c:forEach></td>
			</tr>
		</tbody>
	</table>
</form>
<div class="clear"></div>
<c:choose>
	<c:when test="${not empty order_goods_list }">
		<DIV id="page_wrap">
			<c:forEach var="page" begin="1" end="10" step="1">
				<c:if test="${chapter >1 && page==1 }">
					<a
						href="${pageContext.request.contextPath}/admin/member/adminMemberMain.do?chapter=${chapter-1}&pageNum=${(chapter-1)*10 +1 }">&nbsp;pre
						&nbsp;</a>
				</c:if>
				<a
					href="${pageContext.request.contextPath}/admin/member/adminMemberMain.do?chapter=${chapter}&pageNum=${page}">${(chapter-1)*10 +page }
				</a>
				<c:if test="${page ==10 }">
					<a
						href="${pageContext.request.contextPath}/admin/member/adminMemberMain.do?chapter=${chapter+1}&pageNum=${chapter*10+1}">&nbsp;
						next</a>
				</c:if>
			</c:forEach>
		</DIV>
	</c:when>
</c:choose>
 --%>


<script type="text/javascript">
	//상위코드목록의 row의 시작 번호

	var $memberTable = $('.memberTable').DataTable({
						"processing" : true,
						"serverSide" : true,
						"searching" : false,
						"pagingType" : "full_numbers",
						"ajax" : {
							"url" : "<c:url value='/admin/member/adminMemberList.do'/>",
							"type" : "POST",
							"data" : function(d) {
								//추가 검색인자 설정
								d.searchKeyword = $("#searchKeyword").val();
							}
						},
						"select" : { style : 'single' },
						"columnDefs" : [ {
							"targets" : 7,
							"render" : function(data, type, row, meta) {
								return '<input type="button" value="수정" onclick="jsUpdate(\'' + data + '\')"/>';
								
								
							}
						} ]
					});

	function jsUpdate(id) {
		alert(id);
		
		$.ajax({
		       type:"POST",
		       async:false,  
		       url:"/admin/member/changeAccountStatus.do",
		       contentType : "application/json; charset=utf-8",
		       data: JSON.stringify({member_id:id}),
		       success:function (data,textStatus){
		    	  alert("계정 상태 변경을 완료했습니다.");
		       },
		       error:function(data,textStatus){
		          alert("사용자 정보를 확인해주세요.");
		       },
		    });
		
	}

	
	
<%-- 	
	//조회 버튼 클릭 시 수행
	function fn_detail_search() {
		var frm_delivery_list = document.frm_delivery_list;

		beginYear = frm_delivery_list.beginYear.value;
		beginMonth = frm_delivery_list.beginMonth.value;
		beginDay = frm_delivery_list.beginDay.value;
		endYear = frm_delivery_list.endYear.value;
		endMonth = frm_delivery_list.endMonth.value;
		endDay = frm_delivery_list.endDay.value;
		search_type = frm_delivery_list.s_search_type.value;
		search_word = frm_delivery_list.t_search_word.value;

		var formObj = document.createElement("form");
		var i_command = document.createElement("input");
		var i_beginDate = document.createElement("input");
		var i_endDate = document.createElement("input");
		var i_search_type = document.createElement("input");
		var i_search_word = document.createElement("input");

		i_command.name = "command";
		i_beginDate.name = "beginDate";
		i_endDate.name = "endDate";
		i_search_type.name = "search_type";
		i_search_word.name = "search_word";

		i_command.value = "list_detail_order_goods";
		i_beginDate.value = beginYear + "-" + beginMonth + "-" + beginDay;
		i_endDate.value = endYear + "-" + endMonth + "-" + endDay;
		i_search_type.value = search_type;
		i_search_word.value = search_word;

		formObj.appendChild(i_command);
		formObj.appendChild(i_beginDate);
		formObj.appendChild(i_endDate);
		formObj.appendChild(i_search_type);
		formObj.appendChild(i_search_word);
		document.body.appendChild(formObj);
		formObj.method = "post";
		formObj.action = "/bookstory/admin/member/adminMemberDetail.do";
		formObj.submit();

	}
--%>
	
</script>

