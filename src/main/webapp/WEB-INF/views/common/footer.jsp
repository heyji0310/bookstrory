<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="contextPath"  value="${pageContext.request.contextPath}"  />
<ul>
	<li><a href="#">회사소개</a></li>
	<li><a href="#">이용약관</a></li>
	<li><a href="#">개인정보취급방침</a></li>
	<li><a href="#">제휴/도서홍보</a></li>
	<li><a href="#">광고센터</a></li>
	<li><a href="#">고객만족센터</a></li>
	<li class="no_line"><a href="#">찾아오시는길</a></li>
</ul>
<div class="clear"></div>
<a href="#"><img width="150px"  height="62px" alt="BooKStorY" src="${contextPath}/resources/image/BooKStorY_Logo2.png" /></a>
<div style="padding-left:200px">
	 ㈜북스토리 <br>
	 대표이사: 안혜지   <br>
	 주소 : 우편번호 03133 서울시 종로구 종로3 <br>  
	 사업자등록번호 : 125-81-12345 <br>
	 서울특별시 통신판매업신고번호 : 제 666호 ▶사업자정보확인   개인정보보호최고책임자 : 고길동 privacy@google.co.kr <br>
	 대표전화 : 1577-1577 (발신자 부담전화)   팩스 : 0502-333-3333 (지역번호공통) <br>
	 COPYRIGHT(C) 북스토리 BOOK CENTRE ALL RIGHTS RESERVED.
</div>