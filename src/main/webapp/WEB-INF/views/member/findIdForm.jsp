<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<%
request.setCharacterEncoding("UTF-8");
%>

<div id="container">

	<section id="contents">

		<div id="findId" class="findId">
			<span class="title">아이디 찾기</span>
		</div>
		<form action="<c:url value='/member/findId.do'/>" name="findId"
			id="findId" method="post" enctype="utf-8">
			<label for="member_name" class="title">이름</label> <input type="text"
				id="member_name" name="member_name"
				class="designSettingElement shape" autofocus="autofocus">

			<div class="row">
				<label for="email" class="title">이메일</label> 
				<input type="text" id=email name="email" class="designSettingElement shape">
			</div>

			<div id="findPasswordErrorMsg" class="error-msg"></div>

			<div class="btn-wrapper">
				<button type="submit" class="w-btn-outline w-btn-indigo-outline">아이디 찾기</button>
				<a class="login" href="<c:url value='/member/loginForm.do'/>">로그인
					하러가기</a>
			</div>

		</form>
	</section>
</div>

