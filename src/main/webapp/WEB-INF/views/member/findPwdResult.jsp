<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<%
request.setCharacterEncoding("UTF-8");
%>

<div id="container">

	<section id="contents">

		${message}<br><br>

		<form action="<c:url value='/member/findPwd'/>" name="findPwd" id="findPwd" 
			method="post" enctype="utf-8">
			<div class="btn-wrapper">
				<a href="<c:url value='/member/findPwdForm.do'/>">뒤로가기</a> <a
					href="<c:url value='/member/loginForm.do'/>" class="w-btn-outline w-btn-indigo-outline">로그인 하러가기</a>
			</div>
		</form>
	</section>
</div>
