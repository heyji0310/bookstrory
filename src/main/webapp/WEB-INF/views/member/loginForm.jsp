<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="contextPath"  value="${pageContext.request.contextPath}"  />
<%
  request.setCharacterEncoding("UTF-8");
%>  

<%-- 
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<c:if test='${not empty message }'>
<script>
window.onload=function()
{
  result();
}

function result(){
	alert("아이디나  비밀번호가 틀립니다. 다시 로그인해주세요");
}
</script>
</c:if>
</head>
<body>
	<H3>회원 로그인 창</H3>
	<DIV id="detail_table">
	<form action="${contextPath}/member/login.do" method="post">
		<TABLE>
			<TBODY>
				<TR class="dot_line">
					<TD class="fixed_join">아이디</TD>
					<TD><input name="member_id" type="text" size="20" /></TD>
				</TR>
				<TR class="solid_line">
					<TD class="fixed_join">비밀번호</TD>
					<TD><input name="member_pw" type="password" size="20" /></TD>
				</TR>
			</TBODY>
		</TABLE>
		<br><br>
		<INPUT	type="submit" value="로그인"> 
		<INPUT type="button" value="초기화">
		
		<Br><br>
		   <a href="#">아이디 찾기</a>  | 
		   <a href="#">비밀번호 찾기</a> | 
		   <a href="${contextPath}/member/addMember.do">회원가입</a>    | 
		   <a href="#">고객 센터</a>
					   
	</form>		
</body>
</html>
 --%>

	<div id="container">
		

		<section id="contents">

			<div id="title" class="title">
				<h1>
					<span>로그인</span>
				</h1>
			</div>

			<form action="<c:url value='/member/login'/>" name="loginForm"
				id="loginForm" method="post" enctype="utf-8">
<div class="mainLogin">
				<div>
					<label for="loginId">아이디</label> 
					<input id="loginId" name="loginId"
						type="text" class="login box-size" autofocus="autofocus" style="margin:20px" />
				</div>

				<div>
					<span class="login">비밀번호</span> 
					<input id="loginPwd"
						name="loginPwd" type="password" class="login box-size" style="margin:20px"/>
				</div>

				<div id="loginSignupErrorMsg" class="error-msg"></div>

				<div id="login" class="error-msg"></div>

		<div id="box-outline" align="center" style="width:500px" style="height:180px">
		<div id="box-inline" align="right" style="margin: 30px">
				
				<!-- <div class="button-width" align="center" style="margin: 30px"> -->
					<a class="w-btn-outline w-btn-indigo-outline" 
						href="#" id="loginButton">로그인하기</a>

					<a class="w-btn-outline w-btn-indigo-outline" style="margin: 30px;" style="float:left"
					id="register" href="<c:url value='/member/registerForm.do'/>">회원가입</a>
				</div>


				<div id="findId">
					<a id="findId" href="<c:url value='/member/findIdForm.do'/>">아이디찾기</a>
					<a id="findPwd" href="<c:url value='/member/findPwdForm.do'/>"
						style="margin-left: 20px;">비밀번호 찾기</a>
				</div>
				</div>
				
				</div>
			</form>
			<script type="text/javascript">
			loginButton.addEventListener("click", async e => {
				if (loginId.value == "") {
					alert("아이디를 입력해주세요");
					loginId.focus();
					return false;
				}
				if (loginPwd.value == "") {
					alert("비밀번호를 입력해주세요");
					loginPwd.focus();
					return false;
				}
				const response = await fetch("<c:url value='/member/login.do'/>", {
					method: 'POST'
					, headers: {
						'Content-Type': 'application/json;charset=utf-8'
					}
					, body: JSON.stringify({
						loginId: loginId.value
						, loginPwd: loginPwd.value
					})
				});
				const json = await response.json();
				console.log(json);
				alert(json.message );
				if (json.result) {
					location.href = json.url;							
				}
			});
			</script>


		</section>
	</div>
