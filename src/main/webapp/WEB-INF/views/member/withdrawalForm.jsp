<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<%
request.setCharacterEncoding("UTF-8");
%>

	<section id="contents">
		<div>
			<h1>
				<span id="title">회원 탈퇴</span>
			</h1>
			<br />
		</div>
		<form id="withdrawalForm" name="withdrawalForm">

			<label for="member_id">아이디</label> 
			<input type="text" id="member_id" name="member_id" value="${member.member_id}"
				readonly="readonly" />님 탈퇴하시겠습니까? 
			<br /> <br /> 
			<input type="submit" class="w-btn-outline w-btn-indigo-outline" id="withdrawal" value="탈퇴하기">
			<a href="<c:url value='/mypage/myPageMain.do'/>"
				style="margin-left: 10px">돌아가기</a>



		</form>
	</section>

	<script type="text/javascript">
	window.onload = () => {
	const withdrawal = document.querySelector("#withdrawal");
	const memberId = document.querySelector("#member_id");
	console.log(withdrawal);
	withdrawalForm.addEventListener("submit", async e => {
		e.preventDefault();
		const reqJSON = {
			member_id: member_id.value
		};
		
			fetch("<c:url value='/member/withdrawal.do'/>", {
				method : "POST" ,
				headers : {
					"Content-Type" : "application/json; charset=utf-8",
				},
				body : JSON.stringify(
						reqJSON
				)
			})
			.then(response => response.json())
			.then(jsonObj => {
				alert(jsonObj.message);
				location.href='${contextPath}' + jsonObj.url
			});
		
		
	})
}

</script>